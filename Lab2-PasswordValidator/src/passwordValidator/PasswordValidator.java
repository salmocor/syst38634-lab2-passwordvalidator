/* 
 * Cory Salmon 991526849 
 *  
 * */
package passwordValidator;

/**
 * Password Validator Test class provides static utilities to test the
 * authenticity of a password according to assignment requirements.
 * 
 * This class will ensure that a password is at least 8 characters long and
 * contains 2 digits
 * 
 * @author Cory Salmon
 *
 */
public class PasswordValidator {
	// Moved for readability
	static int minPasswordLength = 8;
	static int minPasswordDigitCount = 2;

	public static boolean isValid(String password) {
		// Moved for readability
		return hasValidLength(password) 
				&& hasValidDigitCount(password) 
				&& hasUpperCase(password)
				&& hasLowerCase(password);
	}

	//Checks that password has upper-case letters
	private static boolean hasUpperCase(String password) {
		for (char character : password.toCharArray()) {
			if (Character.isUpperCase(character)) {
				return true;
			}
		}
		return false;
	}
	
	//Checks that password has lower-case letters
	private static boolean hasLowerCase(String password) {
		for (char character : password.toCharArray()) {
			if (Character.isLowerCase(character)) {
				return true;
			}
		}
		return false;
	}

	//Checks that password has valid length
	private static boolean hasValidLength(String password) {
		// Returns true if length greater than requirement
		if (password.length() >= minPasswordLength) {
			return true;
		}
		return false;
	}

	//Checks that password has valid number of digits
	private static boolean hasValidDigitCount(String password) {
		// Returns true if digits greater than requirement
		if (password.replaceAll("[^0-9]", "").length() >= minPasswordDigitCount) {
			return true;
		}
		return false;
	}
}
