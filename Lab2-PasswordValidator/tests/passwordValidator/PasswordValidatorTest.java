/* Cory Salmon 991526849 */
package passwordValidator;

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {

	@Test
	public void testIsValidRegular() {
		assertTrue("Password was not valid", PasswordValidator.isValid("abCDEFG123"));
	}

	@Test
	public void testIsValidLengthException() {
		assertFalse("Password was valid when it should be invalid", PasswordValidator.isValid("ABC12"));
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		assertTrue("Password was not valid", PasswordValidator.isValid("aBCDEF12"));
	}

	@Test
	public void testIsValidLengthBoundaryOut() {
		assertFalse("Password was valid when it should be invalid", PasswordValidator.isValid("bCDEF21"));
	}
	
	@Test
	public void testIsValidDigitCountException() {
		assertFalse("Password was valid when it should be invalid (Count)", PasswordValidator.isValid("aBCDEFGH"));
	}
	
	@Test
	public void testIsValidDigitCountBoundaryIn() {
		assertTrue("Password was not valid", PasswordValidator.isValid("aBCDEF21"));
	}
	
	@Test
	public void testIsValidDigitCountBoundaryOut() {
		assertFalse("Password was valid when it should be invalid (Count)", PasswordValidator.isValid("aBCDEFG1"));
	}
	
	@Test
	public void testHasValidUppercaseLowercaseException() {
		assertFalse("Password was valid when it should be invalid", PasswordValidator.isValid("4567890123"));
	}
	
	@Test
	public void testHasValidUppercaseLowercaseBoundaryIn() {
		assertTrue("Password was not valid", PasswordValidator.isValid("aB67890123"));
	}
	
	@Test
	public void testHasValidUppercaseLowercaseBoundaryOut() {
		assertFalse("Password was valid when it should be invalid", PasswordValidator.isValid("A567890123"));
	}
	
	@Test
	public void testHasValidUppercaseLowercaseBoundaryOut2() {
		assertFalse("Password was valid when it should be invalid", PasswordValidator.isValid("a567890123"));
	}
}
